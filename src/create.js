let inputContainer = document.getElementById("inputContainer");
let listBox = document.getElementById("listBox");
let statusButtons = document.querySelectorAll(".ulClass li");
let clearAllButton = document.querySelector(".deleteButton");
let allBox=document.getElementById('all');
let activeBox=document.getElementById('active');
let completedBox=document.getElementById('completed');
let box=document.getElementById("box");
inputContainer.focus();
let taskArray = [];


function createToDo() {
    let userInput = inputContainer.value.trim();
    if (userInput) {
        inputContainer.value = "";
        let taskInfo = { name: userInput, status: "active" };
        taskArray.push(taskInfo);
        showAll();
    }
}




statusButtons.forEach((button) => {
    button.addEventListener("click", () => {
        if (button.id === "all") {
            showAll();
        } else if (button.id === "active") {
            showActive();
        } else if (button.id === "completed") {
            showCompleted();
        }
    });
});





function deleteLogo(input) {
    let data = input.parentElement
    taskArray.splice(data.id, 1);
    showCompleted();
}

function strikeThrough(selectedTask) {
    let taskName = selectedTask.parentElement;
    if (selectedTask.checked) {
        taskName.classList.add("text");
        taskArray[selectedTask.id].status = "completed";
    } else {
        taskName.classList.remove("text");
        taskArray[selectedTask.id].status = "active";
        showAll();
    }
    taskArray = [...taskArray];
}



clearAllButton.addEventListener("click", () => {
    const temp = taskArray.filter((element) => element.status === "active");
    taskArray = [...temp];
    showCompleted();
});