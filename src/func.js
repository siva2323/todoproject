function showAll() {
    inputContainer.focus()
    clearAllButton.style.display = "none";
    let storage = "";
    if (taskArray) {
        taskArray.forEach((todo, key) => {
            let isCompleted = todo.status === "completed" ? "checked" : "";
            let isCompleted1 = todo.status === "completed" ? "text" : "";
            storage += `<div class="createdContainer" id=${key}>
                <label for=${key} class="labelElement ${isCompleted1}">
                    <input type="checkbox" id=${key} class="checkbox" onclick="strikeThrough(this)" ${isCompleted}>
                    ${todo.name}
                </label>
                <i class="material-icons buttonImg hidden" onclick="deleteLogo(this)" id=${key}>delete</i>
            </div>`;
        });
        let deleteButtonList = document.querySelectorAll(".material-icons");
        deleteButtonList.forEach((button) => {
            button.classList.add("hidden");
        });
    }
    listBox.innerHTML = storage;
}

function showActive() {
    clearAllButton.style.display = "none";
    listBox.innerHTML = "";
    let storage = "";
    if (taskArray) {
        taskArray.forEach((todo, key) => {
            let isCompleted = todo.status === "completed" ? "checked" : "";
            let isCompleted1 = todo.status === "completed" ? "text" : "";
            if (todo.status === "active") {
                storage += `<div class="createdContainer" id=${key}>
                    <label for=${key} class="labelElement ${isCompleted1}">
                        <input type="checkbox" id=${key} class="checkbox" onclick="strikeThrough(this)" ${isCompleted}>
                        ${todo.name}
                    </label>
                    <i class="material-icons buttonImg hidden" onclick="deleteLogo(this)" id=${key}>delete</i>
                </div>`;
            }
        });
        let deleteButtonList = document.querySelectorAll(".material-icons");
        deleteButtonList.forEach((button) => {
            button.classList.add("hidden");
        });
    }
    listBox.innerHTML = storage;
}
function showCompleted() {
    clearAllButton.style.display = "block";
    listBox.innerHTML = "";
    let storage = "";
    if (taskArray) {
        taskArray.forEach((todo, key) => {
            let isCompleted = todo.status === "completed" ? "checked" : "";
            let isCompleted1 = todo.status === "completed" ? "text" : "";
            if (todo.status === "completed") {
                clearAllButton.style.display = "block";
                storage += `<div class="createdContainer" id=${key}>
                    <label for=${key} class="labelElement ${isCompleted1}">
                        <input type="checkbox" id=${key} class="checkbox" onclick="strikeThrough(this)" ${isCompleted}>
                        ${todo.name}
                    </label>
                    <i class="fa fa-trash buttonImg" onclick="deleteLogo(this)" id=${key} aria-hidden="true"></i>
                </div>`;
            }
        });
    }
    listBox.innerHTML = storage;
}

