## Technologies used in project 


* HTML
* CSS
* JavaScript 


## Description 


The application has three tabs: 

- All tasks: Displays all of the tasks, both active and completed.
- Active: Displays all tasks that are currently underway.
- Completed: Displays all of the tasks that have been finished.



